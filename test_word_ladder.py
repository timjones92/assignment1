import unittest
import word_ladder


class WordLadderTestCases(unittest.TestCase):
    
    # Test wrong file name function
    
    def test_file_name(self):
        result = word_ladder.wordLadderInputs("dictionry.txt")
        self.assertTrue(result == "Wrong file name. Please try again.")
    
    # Test empty input file
    
    def test_empty_input_file(self):
        result1 = word_ladder.wordLadderInputs("")
        self.assertEqual(result1, "File name cannot be empty.")
    
    # Test if user input for start word is a number
    
    def test_start_not_int(self):
        word_ladder.wordLadderInputs("dictionary.txt")
        result2 = 3
        self.assertNotEqual(result2, True)
    
    # Test if user input for target word is a number
    
    def test_target_not_int(self):
        word_ladder.wordLadderInputs("dictionary.txt")
        result3 = 3
        self.assertNotEqual(result3, True)
    
    # Test user input for start word that is not in dictionary
    
    def test_start_not_in_dict(self):
        self.assertNotEqual(word_ladder.solution("testatf", "teasetee", ""), True)
    
    # Test user input for target word that is not in dictionary
    
    def test_target_not_in_dict(self):
        result4 = word_ladder.solution("test", "tesstt", "")
        self.assertNotEqual(result4, True)
    
    # Test if item and target word are not same length
    
    def test_length_item_target(self):
        result5 = word_ladder.same("test", "tents")
        self.assertNotEqual(result5, True)
    
    # Test if word is seen and not in list (of dictionary words)
    
    def test_seen_and_not_in_list(self):
        result6 = word_ladder.build("", "test", {"": True}, "")
        self.assertNotEqual(result6, True)
    
    # Test if match_word is empty
    
    def test_match_empty(self):
        result7 = word_ladder.shortPath("test", "dictionary.txt", "", "", "tail")
        self.assertNotEqual(result7, True)
    
    # Test if match does not equal target
    
    def test_match_not_equal_target(self):
        result8 = word_ladder.longPath("test", "", "", "", "tail")
        self.assertNotEqual(result8, True)


if __name__ == '__main__':
    unittest.main()
