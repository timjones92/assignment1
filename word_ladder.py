import re


def same(item, target):
    return len([c for (c, t) in zip(item, target) if c == t])


def build(pattern, words, seen, list):
    return [word for word in words
            if re.search(pattern, word) and word not in seen.keys() and
            word not in list]


# Function defining the shortest path
def shortPath(word, words, seen, target, path):
    while True:
        match_word = []
        for i in range(len(word)):
            match_word += build(word[:i] + "." + word[i + 1:], words, seen, match_word)
        if len(match_word) == 0:
            return False
        # this list prints all occurrences where the string changes at each character index
        match_word = sorted([(same(w, target), w) for w in match_word], reverse=True)
        # using same function to compare words in list and target word
        # list is sorted alphabetically
        # key = number of letters that are the same in the word in the list and the target word
        # value = each word in the list
        for (match, item) in match_word:
            # if key is greater than or equal to target length - 1
            if match >= len(target) - 1:
                # if match (key in list tuple) is equal to length target
                # append item to path
                if match == len(target) - 1:
                    path.append(item)
                return True
            seen[item] = True
        # this for loop occurs when the above function isn't met
        for (match, item) in match_word:
            if match < 1:
                return False
            path.append(item)
            if shortPath(item, words, seen, target, path):
                return True
            path.pop()


# Function defining the longest path
def longPath(word, words, seen, target, path):
    while True:
        match_word = []
        for i in range(len(word)):
            match_word += build(word[:i] + "." + word[i + 1:], words, seen, match_word)
        if len(match_word) == 0:
            return False
        # this list prints all occurrences where the string changes at each character index
        match_word = sorted([(same(w, target), w) for w in match_word])
        # using same function to compare words in list and target word
        # list is sorted alphabetically
        # key = number of letters that are the same in the word in the list and the target word
        # value = each word in the list
        for (match, item) in match_word:
            # if key is greater than or equal to target length - 1
            if match >= len(target) - 1:
                # if match (key in list tuple) is equal to length target
                # append item to path
                if match == len(target) - 1:
                    path.append(item)
                return True

            seen[item] = True
        # this for loop occurs when the above function isn't met
        for (match, item) in match_word:
            path.append(item)
            if longPath(item, words, seen, target, path):
                return True
            path.pop()


# Function asks user to input start word and target word
def solution(start, target, lines):
    while True:
        # Empty list to start with for variable words
        words = []
        # Appends words from the file and into a new list of words
        # with equal character length to the start word
        for line in lines:
            word = line.rstrip()
            if len(word) == len(start):
                words.append(word)
        avoid = input("Enter any words you wish to be avoided in the path (separated by a comma)."
                      "\nWhen ready, press enter to continue: ")
        remove = [x.strip() for x in avoid.split(',')]
        for word in list(words):
            if word in remove:
                words.remove(word)
        break
    path = [start]
    seen = {start: True}
    # Uses the find function to return the number of words in the path
    # and return all the words in the path
    while True:
        shortWay = input("Would you like to find the shortest path? Enter y/n: ")
        if shortWay == "y":
            if shortPath(start, words, seen, target, path):
                path.append(target)
                return len(path) - 1, path
            else:
                return "No path found"
        elif shortWay == "n":
            if longPath(start, words, seen, target, path):
                path.append(target)
                return len(path) - 1, path
            else:
                return "No path found"
        else:
            print("Input must be y or n only. Please try again.")
            continue


# Function to open file name and read all words in the file
def wordLadderInputs(fileName):
    while True:
        try:
            file = open(fileName)  # Try opening the file
            lines = file.readlines()  # Read lines from the file

            # Word Ladder start word for user to input
            start = input("Enter start word: ")

            # Start word error handling
            try:
                int(start)  # Check if start word is an integer type
            except ValueError:
                if not re.match("^[a-zA-Z_]*$", start):  # Check if start word contains special characters
                    print("Start word cannot contain numbers or symbols, please try again.")
                    continue
                elif (start + "\n") not in lines:  # If start word is not in the dictionary
                    print("Start word not in dictionary, please try again.")  # Return error message
                    continue
                else:
                    print("Valid input, your starting word is:",
                          start)  # Print statement if start word not an integer type
            else:
                print("Start word cannot be a number, please try again.")  # Return error message if type = int
                continue

            # Word Ladder target word for user to input
            target = input("Enter target word: ")

            # Target word error handling
            if (start + "\n") not in lines:  # If target word is not in the dictionary
                print("Start word not in dictionary, please try again.")  # Return error message
                continue
            try:
                int(target)  # Check if the target word is an integer type
            except ValueError:
                if not re.match("^[a-zA-Z_]*$", target):
                    print(
                        "Target word cannot contain numbers or symbols, please try again.")  # Check if target word contains special characters
                    continue
                elif (target + "\n") not in lines:  # If target word is not in the dictionary
                    print("Start word not in dictionary, please try again.")  # Return error message
                    continue
                else:
                    print("Valid input, your starting word is:",
                          target)  # Print statement if target word not an integer type
            else:
                print("Target word cannot be a number, please try again.")  # Return error message if type = int
                continue

            # Error handling if length of start and target word are not equal
            if len(start) != len(target):
                print("Length of start and target words must be the same, please try again.")
                continue

            # Return solution function if all conditions met
            return solution(start, target, lines)

        # Error handlers for the file name being incorrect
        except:
            if len(fileName) < 1:  # If length is less than 1
                return "File name cannot be empty."  # Returns error message for empty file
            elif fileName != "dictionary.txt":  # If user inputs wrong file name
                return "Wrong file name. Please try again."  # Returns error message for wrong file name
            else:
                break


# Ask user to input name of file
print(wordLadderInputs(input("Enter dictionary name: ")))